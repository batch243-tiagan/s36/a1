const express = require('express')

//Create a Router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require("../controllers/main-controllers");

	router.get("/:id",(req,res)=>{
		taskController.findTaskByID(req.params.id).then(resultFromController => res.send(resultFromController));
	})

	router.put("/:id",(req, res)=>{
		taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
	});


module.exports = router;