const Task = require("../models/task");

	module.exports.findTaskByID = (id) => {
				try{
					return Task.findById(id);
				}catch(err){
					console.log(err);
					return false;
				}
			}

	module.exports.updateTask =(id, jsonObj) =>{
					return Task.findById(id).then((result, error) =>{
						if(error){
							console.log(error)
							return false
						}
							result.status = jsonObj.status;

							return result.save().then((updatedTask, saveErr) =>{

								if(saveErr){
									console.log(saveErr)
									return false
								}else{

									return updatedTask
								}
							})		
				})
			}
